require 'opencv'
include OpenCV

class Image < ActiveRecord::Base


  def getBinaryString(imageArray)

    #image.save_image(ARGV[1])
    encodedImage = imageArray.encode_image('.png', CV_IMWRITE_PNG_COMPRESSION => 1)

    #16進数へ
    mapped = encodedImage.map! { |s| "%02X" % s }
    #結合して一つの文字列へ
    msg = []
    msg << mapped.join
    bin = msg.pack("H*")
    return bin
  end


 def getFace

   data = Rails.root.to_s + "/haarcascade_frontalface_alt.xml"
   detector = CvHaarClassifierCascade::load(data)

   #image = CvMat.load(ARGV[0])
   imageArray = CvMat.decode_image(self.imageData, iscolor = 1)

   detector.detect_objects(imageArray).each do |region|
     color = CvColor::Blue
     imageArray.rectangle! region.top_left, region.bottom_right, :color => color
   end

   return getBinaryString(imageArray)

 end



 def getPose


 end

  def getSegment


    threshold1 = 255.0
    threshold2 = 80.0
    level = 4

    imageIpl = IplImage.decode_image(self.imageData, iscolor = 1)


    width = imageIpl.width  & -(1 << level);
    height = imageIpl.height & -(1 << level);
    roi = CvRect.new(0, 0 ,width, height)



    imageIpl.set_roi(roi)

    segmented = imageIpl.pyr_segmentation(level, threshold1, threshold2)


    return getBinaryString(segmented[0])
  end

end
