
var Pose = {
	IMAGE_WIDTH:320,
	IMAGE_HEIGHT:240,
	videoElement:null, //いろんなところで使うビデオ要素
	flgGetBackground:false,
    processing:false,
    getCanvas:function(){
        var resultCanvas = document.createElement('canvas');
        resultCanvas.setAttribute("width", Pose.IMAGE_WIDTH);
        resultCanvas.setAttribute("height", Pose.IMAGE_HEIGHT);
        return resultCanvas;
    }
}


$(document).ready(function(){
	initCamera();
	setEvents();
	background.init();
});


//インジゲータ操作用オブジェクト
var indigator = {
	on:function(){
		jIndigator = this.init();
        $("#instruction").hide();
        $("#count").hide();
		jIndigator.show().animate({
			height:"40px"
		});
	},
	off:function(){
		jIndigator = this.init();
		jIndigator.animate({
			height:"0"
		},function(){
			jIndigator.attr("height",0);
			jIndigator.hide();
            $("#instruction").show();
		});
		
	
	},
	init:function(){
		return $("#indigator");
	}

}

//背景撮影済みフラグ管理用オブジェクト
var background = {
	get:function(){
		$("#getPose").removeClass("disabled");
		 Pose.flgGetBackground = true;

	},
	init:function(){
		$("#getPose").addClass("disabled");
		 Pose.flgGetBackground = false;
	}
}

//webカメラの初期化
function initCamera(){
    Pose.videoElement = $("#video").get(0);
    var videoObj = { "video": true };
    var falseCallback = function(error) {
        console.log("Video capture error: ", error.name);
    };

    Pose.videoElement.setAttribute("width",Pose.IMAGE_WIDTH);
    Pose.videoElement.setAttribute("height",Pose.IMAGE_HEIGHT);

	//ビデオリスナーのセット
    if(navigator.getUserMedia) { // Standard
        navigator.getUserMedia(videoObj, function(stream) {
            Pose.videoElement.src = stream;
            Pose.videoElement.play();
        }, falseCallback);
    } else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
        navigator.webkitGetUserMedia(videoObj, function(stream){
            Pose.videoElement.src = window.webkitURL.createObjectURL(stream);
            Pose.videoElement.play();
        }, falseCallback);
    } else if(navigator.mozGetUserMedia) { // Firefox-prefixed
        navigator.mozGetUserMedia(videoObj, function(stream){
            Pose.videoElement.src = window.URL.createObjectURL(stream);
            Pose.videoElement.play();
        }, falseCallback);
    }

}

//ボタンクリックイベント系
function setEvents(){

    //撮影
    $("#snap").on("click", function() {

        delayedSnap();

        /*
        var canvas = snapPhoto();
        background.init();

        //setImageUrl(canvas);

        sendImage(canvas);
        */
       
    });


}

//撮影関数
function snapPhoto(){
    //canvasに描画
    var canvas = document.createElement('canvas');
    canvas.setAttribute("width", Pose.IMAGE_WIDTH);
    canvas.setAttribute("height", Pose.IMAGE_HEIGHT);
    context = canvas.getContext("2d"),
    context.drawImage(Pose.videoElement, 0, 0, Pose.IMAGE_WIDTH, Pose.IMAGE_HEIGHT);

    //$("#results").empty();//キャンバスクリア
    //$("#results").append(canvas);

    return canvas;
}


function delayedSnap(){
    if(Pose.processing == true){
        return false;
    }
    Pose.processing = true;

    var count = 3;
    var counter = function(){
        $("#count").html(count);
        indigator.on();
        $("#count").show();

        if(count == 0){
            clearTimeout(timer1);

            var canvas = snapPhoto();
            background.init();

            sendImage(canvas);

            indigator.off();
            Pose.processing = false;

        }
        count--;
    }
    timer1 = setInterval(counter,1000);


}

//画像の表示
function setImageUrl(canvas){
    var image = new Image();
    var imageUrl = canvas.toDataURL("image/png");
 
    $("a#downloadImage").attr("href", imageUrl);
    $("a#downloadImage").show();
}

//画像の送信
function sendImage(canvas){
    var imgData = canvas.toDataURL("image/png");
    imgData = imgData.replace(/^.*,/, '');
    $("#imageData_1").val(imgData);
    //console.log(imgData);

    var valuesToSubmit = $("#imageForm").serialize();
    $.ajax({
        type: "POST",
        url: "/main/silhouette",
        data: valuesToSubmit,
        success: function(data){

            //受け取ったデータの処理
            var image = new Image();
            image.src = "data:image/png;base64," +  data;
            $("#results").empty();
            $("#results").append(image);

        }
    });

}
