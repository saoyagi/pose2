require 'base64'


require 'pp'



class MainController < ApplicationController
  def index
  end

  def silhouette
    imageData = Base64.decode64 params["imageData"]["1"]


    #画像処理
    sentImage = Image.new ({imageData: imageData})
    #resultImage = sentImage.getFace
    resultImage = sentImage.getSegment

    imageString = Base64.encode64 resultImage
    render :text => imageString
  end
end
